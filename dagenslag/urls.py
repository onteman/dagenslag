from django.conf.urls import patterns, include, url
from django.contrib import admin
import settings
from dagenslag_app.views import api
from dagenslag_app.views import nojs 

from django.conf.urls.static import static

urlpatterns = patterns('',
                       
    url(r'nojs/?$', nojs.Games.as_view(), name = 'nojs_games'),
    url(r'nojs/(?P<id>\d+)/?$', nojs.Game.as_view(), name = 'nojs_game'),
    url(r'nojs/signup/(?P<id>\d+)/?$', nojs.Signups.as_view(), name = 'nojs_signup'),
    url(r'nojs/players/?$', nojs.Players.as_view(), name = 'nojs_players'),
    url(r'nojs/player/(?P<id>\w+)/?$', nojs.Player.as_view(), name = 'nojs_player'),
    url(r'nojs/teams/?$', nojs.Teams.as_view(), name = 'nojs_teams'),


    url(r'api/games/?$', api.Games.as_view(), name = 'api_games'),
    url(r'api/game/(?P<id>\d+)/?$', api.Game.as_view(), name = 'api_game'),
    url(r'api/players/?$', api.Players.as_view(), name = 'api_players'),
    url(r'api/player/(?P<id>\w+)/?$', api.Player.as_view(), name = 'api_player'),
    url(r'api/teams/(?P<num_teams>\d+)/(?P<players>[\w,]+)/?$', api.Teams.as_view(), name = 'api_teams'),
    url(r'api/gametypes/?$', api.GameTypes.as_view(), name = 'api_gametypes'),
    url(r'api/signup/(?P<id>\d+)/?$', api.Signup.as_view(), name = 'api_signup'),

    url(r'admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += static('/dagenslag/', document_root = 'dagenslag_app/static/angular')
