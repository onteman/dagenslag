import itertools
import random
import re
import datetime
import json 

from django.views.generic import View
from django.core import serializers
from django.http import Http404, QueryDict, HttpResponseRedirect
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response, render
from django.views.decorators.csrf   import csrf_exempt
from django.core.cache import caches

import dagenslag_app.models as models

class Games(View):

    def get(self, request):
        cache = caches['default']
            
        resp = cache.get('home')
        if resp:
            return resp
        
        games = models.Game.objects.order_by('-date').filter(date__year=datetime.date.today().year)
        for game in games:
            signups = game.signups.all()
            game.unsigned = models.Player.objects.exclude(id__in=signups).order_by('name')

        resp = render_to_response('nojs/index.html', { 'games' : games } )
        cache.set('home', resp)
        
        return resp


class Game(View):

    def get(self, request, id):
        game_key = id.encode('utf-8')

        game = models.Game.objects.get(id = game_key)
        resp = render_to_response('nojs/modify_game.html', { 'game' : game } )

        return resp


class Signups(View):

    def post(self, request, id):
        cache = caches['default']
        
        put = QueryDict(request.body)

        game_key = id.encode('utf-8')
        game = models.Game.objects.get(id = game_key)
        
        nick = put.get('player')
        player = models.Player.objects.get(nickname = nick)
        
        game.signups.add(player)
        
        cache.clear()
        return HttpResponseRedirect('/nojs')
           

class Players(View):

    def get(self, request):
        cache = caches['default']

        resp = cache.get('handle_players')
        if resp:
            return resp
        
        players = list(models.Player.objects.all())
        
        for player in players:
            player.get_stats(True)
        
        players.sort(key = lambda p: (-p.stats['total']['win_ratio'], -p.stats['total']['games_played'], p.name))
        
        
        selected = request.GET.getlist('selected')
        for player in players:
            if player.nickname in selected:
                player.selected = True


        gametypes = set()
        for player in players:
            for gametype in player.stats.keys():
                gametypes.add(gametype)
        
        gametypes = sorted(list(gametypes), key = lambda x: (x == 'total', x), reverse = True)
        
        for player in players:
            stats_by_gametype = []
            for gametype in player.stats.keys():
                stat = player.stats[gametype]
                stat['gametype'] = gametype
                stats_by_gametype.append(stat)
             
            for gametype in gametypes:
                if not gametype in player.stats.keys():
                    stat = {}
                    stat['games_won'] = 0 
                    stat['games_played'] = 0
                    stat['games'] = []
                    stat['win_ratio'] = 0
                    stat['gametype'] = gametype
                    stats_by_gametype.append(stat)
                    
            stats_by_gametype = sorted(stats_by_gametype, key = lambda x: (x['gametype'] == 'total', x['gametype']), reverse = True)
            player.stats = stats_by_gametype
        
        resp = render_to_response('nojs/players.html', { 'players' : players, 'gametypes' : gametypes } )
        cache.set('handle_players', resp)
        
        return resp


class Player(View):

    def get(self, request, id):
        cache = caches['default']
        
        nick = id.encode('utf-8')

        resp = cache.get('handle_player_' + nick)
        if resp:
            return resp
        
        player = models.Player.objects.get(nickname = nick)
        player.get_stats(True)
        
        resp = render_to_response('nojs/player.html', { 'player' : player } )
        cache.set('handle_player_' + nick, resp)
        
        return resp
    

class Teams(View):

    def get(self, request):
        selected = request.GET.getlist('players')
        
        num_teams = request.GET.get('num_teams', 2)
        num_teams = int(num_teams)
        if num_teams < 2 or num_teams > 10:
            num_teams = 2
        
        teams = models.Player.generate_teams(selected, num_teams)

        return render_to_response('nojs/teams.html', { 'teams' : teams, 'selected' : selected })
