import itertools
import re
import datetime
import json 

from django.views.generic import View
from django.core import serializers
from django.http import Http404, QueryDict
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response, render
from django.core.cache import caches

import dagenslag_app.models as models

class PlayerSerializerJSON(serializers.json.Serializer):

    def get_dump_object(self, obj):
        self._current['id'] = obj._get_pk_val()

        try:
            self._current['stats'] = obj.stats
        except AttributeError:
            pass

        return self._current

class PlayerSerializerPython(serializers.python.Serializer):

    def get_dump_object(self, obj):
        self._current['id'] = obj._get_pk_val()

        try:
            self._current['stats'] = obj.stats
        except AttributeError:
            pass

        return self._current

class GameSerializer(serializers.json.Serializer):

    def get_dump_object(self, obj):
        self._current['id'] = obj._get_pk_val()

        p = PlayerSerializerPython()
        self._current['team_0'] = p.serialize(obj.team_0.all())
        self._current['team_1'] = p.serialize(obj.team_1.all())
        self._current['signups'] = p.serialize(obj.signups.all())
        self._current['unsigned'] = p.serialize(obj.unsigned)
        return self._current


class Games(View):

    date_filters = ('all', 'year')

    def get(self, request):
        cache = caches['games']

        date_filter = request.GET.get('filter', 'all')
        if date_filter not in Games.date_filters:
            date_filter = 'all'

        resp = cache.get('games_api_' + date_filter)
        if resp:
            return resp

        games = cache.get('games_query_' + date_filter, None)
        if not games:
            if date_filter == 'year': 
                games = models.Game.objects.order_by('-date').filter(date__year=datetime.date.today().year)
            else:
                games = models.Game.objects.order_by('-date').all()
            cache.set('games_query_' + date_filter, games)

        for game in games:
            signups = game.signups.all()
            game.unsigned = models.Player.objects.exclude(id__in=signups).order_by('name')

        s = GameSerializer()
        items = s.serialize(games)
        resp = HttpResponse(items, content_type='application/json')
        cache.set('games_api_' + date_filter, resp)

        return resp

    def post(self, request):

        body = {}
        
        try:
            body = json.loads(request.body)
        except:
            return HttpResponseBadRequest('Invalid json data', content_type='application/json')
            

        date = body.get('date')
        if not date:
            return HttpResponseBadRequest('Missing required parameter: date', content_type='application/json')
        
        gametype = body.get('gametype')
        if not gametype:
            return HttpResponseBadRequest('Missing required parameter: gametype', content_type='application/json')
        
        
        signups = body.get('signups', [])
        signups_players = []
        for nickname in signups:
            player = models.Player.objects.get(nickname = nickname)
            signups_players.append(player)
        
        team_0 = body.get('team_0', [])
        team_0_players = []
        for nickname in team_0:
            player = models.Player.objects.get(nickname = nickname)
            team_0_players.append(player)
        
        team_1 = body.get('team_1', [])
        team_1_players = []
        for nickname in team_1:
            player = models.Player.objects.get(nickname = nickname)
            team_1_players.append(player)
        
        
        winning_team = body.get('winning_team')
        if winning_team != 0 and winning_team != 1:
            winning_team = -1
        
        
        game = models.Game(date = date, type = gametype, winning_team = winning_team )
        game.save()
        for player in signups_players:
            game.signups.add(player)
        for player in team_0_players:
            game.team_0.add(player)
        for player in team_1_players:
            game.team_1.add(player)
        
        caches['games'].clear()
        caches['players'].clear()
        caches['player'].clear()
        return HttpResponse('Game added', content_type='application/json')

class Game(View):

    def get(self, request, id):
        cache = caches['game']

        game_key = id.encode('utf-8')

        resp = cache.get('game_' + game_key + '_api')
        if resp:
            return resp

        game = cache.get('game_'+ game_key + '_query', None)
        if not game:
            game = models.Game.objects.get(id = game_key)
            cache.set('game_'+ game_key + '_query', game)

        signups = game.signups.all()
        game.unsigned = models.Player.objects.exclude(id__in=signups).order_by('name')

        s = GameSerializer()
        items = s.serialize([game])
        resp = HttpResponse(items, content_type='application/json')
        cache.set('game_' + game_key + '_api', resp)

        return resp

    def put(self, request, id):
        game_key = id.encode('utf-8')
        game = models.Game.objects.get(id = game_key)
     
        body = {}
    
        try:
            body = json.loads(request.body)
        except:
            return HttpResponseBadRequest('Invalid json data', content_type='application/json')

        date = body.get('date')
        gametype = body.get('gametype')
        
        signups = body.get('signups', [])
        signups_players = []
        for nickname in signups:
            player = models.Player.objects.get(nickname = nickname)
            signups_players.append(player)
        
        team_0 = body.get('team_0', [])
        team_0_players = []
        for nickname in team_0:
            player = models.Player.objects.get(nickname = nickname)
            team_0_players.append(player)
        
        team_1 = body.get('team_1', [])
        team_1_players = []
        for nickname in team_1:
            player = models.Player.objects.get(nickname = nickname)
            team_1_players.append(player)
        
        
        winning_team = body.get('winning_team')
        if winning_team != 0 and winning_team != 1 and winning_team is not None:
            winning_team = -1
        
        
        if date:
            game.date = date
        if gametype:
            game.type = gametype
        if winning_team:
            game.winning_team = winning_team
        game.save()
        
        
        if signups_players:
            game.signups.clear()
        for player in signups_players:
            game.signups.add(player)
            
        if team_0_players:
            game.team_0.clear()
        for player in team_0_players:
            game.team_0.add(player)
        
        if team_1_players:
            game.team_1.clear()
        for player in team_1_players:
            game.team_1.add(player)
        
        caches['games'].clear()
        caches['game'].clear()
        caches['players'].clear()
        caches['player'].clear()
        return HttpResponse('Game modified', content_type='application/json')

class Players(View):

    def get(self, request):
        cache = caches['players']

        filter_by_year = False
        date_filter = request.GET.get('filter', '')
        if date_filter == 'year':
            filter_by_year = True

        resp = cache.get('handle_players_api_' + 'year' if filter_by_year else 'all')
        if resp:
            return resp
            

        players = list(models.Player.objects.all())
        for player in players:
            player.get_stats(filter_by_year)

        s = PlayerSerializerJSON()
        items = s.serialize(players)
        resp = HttpResponse(items, content_type='application/json')
        cache.set('handle_players_api_' + 'year' if filter_by_year else 'all', resp)
        
        return resp

    def post(self, request, *args, **kwargs):
        cache = caches['players']

        body = {}

        try:
            body = json.loads(request.body)
        except:
            return HttpResponseBadRequest('Invalid json data', content_type='application/json')
        
        
        nickname = body.get('nickname')
        if not nickname:
            return HttpResponseBadRequest('Missing required parameter: nickname', content_type='application/json')
        
        name = body.get('name')
        if not name:
            return HttpResponseBadRequest('Missing required parameter: name', content_type='application/json')
        
        player = models.Player(name = name, nickname = nickname)
        player.save()
        
        cache.clear()
        return HttpResponse(json.dumps({ 'nickname' : nickname, 'name' : name }), content_type='application/json')

class Player(View):

    def get(self, request, id):
        cache = caches['player']

        nick = id.encode('utf-8')

        resp = cache.get('handle_player_' + nick + '_api')
        if resp:
            return resp
        
        player = models.Player.objects.get(nickname = nick)
        player.get_stats()

        s = PlayerSerializerJSON()
        items = s.serialize([player])
        
        resp = HttpResponse(items, content_type='application/json')
        cache.set('handle_player_' + nick + '_api', resp)
        
        return resp

class Teams(View):

    def get(self, request, num_teams, players):

        teams = models.Player.generate_teams(players.split(','), int(num_teams))

        s = PlayerSerializerPython()
        for team in teams:
            team['players'] = s.serialize(team['players'])

        return HttpResponse(json.dumps(teams), content_type='application/json')

class GameTypes(View):

    def get(self, request):
        cache = caches['gametypes']

        resp = cache.get('gametypes_api')
        if resp:
            return resp
        
        gametypes = models.Game.objects.values_list('type', flat = True)
        gametypes = set(gametypes)
        gametypes.add('total')

        gametypes = sorted(list(gametypes), key = lambda x: (x == 'total', x), reverse = True)
        
        resp = HttpResponse(json.dumps(gametypes), content_type='application/json')
        cache.set('gametypes_api', resp)
        
        return resp

class Signup(View):

    def post(self, request, id):
        body = {}

        try:
            body = json.loads(request.body)
        except:
            return HttpResponseBadRequest('Invalid json data', content_type='application/json')
        

        nickname = body.get('nickname')
        if not nickname:
            return HttpResponseBadRequest('Missing required parameter: nickname', content_type='application/json')


        game_key = id.encode('utf-8')

        game = caches['game'].get('game_'+ game_key + '_query', None)
        if not game:
            game = models.Game.objects.get(id = game_key)
        
        player = models.Player.objects.get(nickname = nickname)
        
        game.signups.add(player)
        

        # update cached games with the new signup
        for date_filter in Games.date_filters:
            games = caches['games'].get('games_query_' + date_filter, None)
            if games:
                games = [g if g.id != game.id else game for g in games]
                caches['games'].set('games_query_' + date_filter, games)
                caches['games'].set('games_api_' + date_filter, None)


        caches['game'].set('game_'+ game_key + '_query', game)
        caches['game'].set('game_'+ game_key + '_api', None)

        
        return HttpResponse('Game modified', content_type='application/json')
