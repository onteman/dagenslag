from django.db import models
import itertools
import datetime
import random

class Player(models.Model):
    nickname = models.CharField(max_length = 60, unique = True)
    name = models.CharField(max_length = 220)
    organizer = models.BooleanField(default = False)
    gmail = models.CharField(max_length = 128, default = '')
   
   
   
    def get_stats(self, filter_by_year = False):

        team_0_games = None
        team_1_games = None

        
        if filter_by_year:
            team_0_games = self.team_0.filter(date__year=datetime.date.today().year)
            team_1_games = self.team_1.filter(date__year=datetime.date.today().year)
        else:
            team_0_games = self.team_0.all()
            team_1_games = self.team_1.all()
        
        
        for game in team_0_games:
            game.won = (game.winning_team == 0)
        for game in team_1_games:
            game.won = (game.winning_team == 1)
        
        games = list(itertools.chain(team_0_games, team_1_games))
        
        stats = {}
        for game in games:
            if not stats.get(game.type):
                stats[game.type] = {}
            if not stats[game.type].get('games_won'):
                stats[game.type]['games_won'] = 0
            if not stats[game.type].get('games_played'):
                stats[game.type]['games_played'] = 0
            
            if game.won:
                stats[game.type]['games_won'] += 1
                
            stats[game.type]['games_played'] += 1    
            stats[game.type]['win_ratio'] = 0
        
        
        

        stats['total'] = {}
        stats['total']['games_won'] = 0 
        stats['total']['games_played'] = 0
        stats['total']['win_ratio'] = 0

        
        for stat in stats.values():
            if stat['games_played'] > 0:
                stat['win_ratio'] = int((float(stat['games_won']) / float(stat['games_played'])) * 100.0)
              
            stats['total']['games_won'] += stat['games_won']
            stats['total']['games_played'] += stat['games_played']
            
        if stats['total']['games_played'] > 0:
            stats['total']['win_ratio'] = int((float(stats['total']['games_won']) / float(stats['total']['games_played'])) * 100.0)
        
        
        self.stats = stats
       

    @staticmethod
    def generate_teams(selected, num_teams = 2):

        today = datetime.date.today()
        random.seed(today)
    
        players = list(Player.objects.filter(nickname__in = selected))
        for player in players:
            player.get_stats()
    
        #divide player in groups based on win ratios, 100-75%, 75-50%, 50-25%, 25-0% and unranked
        random_players = []
        intervals = [(75, 101), (50, 75), (25,50), (0,25)]
        for interval in intervals:
            min_ratio, max_ratio = interval
            players_interval = [p for p in players if p.stats['total']['win_ratio'] >= min_ratio and p.stats['total']['win_ratio'] < max_ratio and p.stats['total']['games_played'] > 0] 

            random.shuffle(players_interval)

            random_players.extend(players_interval)

        unranked = [p for p in players if p.stats['total']['games_played'] == 0]
        random.shuffle(unranked)
        
        random_players.extend(unranked)

        players = random_players
        
        teams = []
        for i in xrange(0, num_teams):
            teams.append({ 'players' : [] })
        
        
        
        for team in teams:
            if len(players)%len(teams) != 0:
                player = players.pop()
                team['players'].append(player)
                
        while players:
            for team in teams:
                player = players.pop()
                team['players'].append(player)
        
        for team in teams:
            team['players'].reverse()
        


        #calculate team stats
        for team in teams:
            stats = {}
            games_won = 0
            games_played = 0
             
             
            for player in team['players']:
                games_won += player.stats['total']['games_won']
                games_played += player.stats['total']['games_played']
        
        
            stats['games_won'] = games_won
            stats['games_played'] = games_played
            stats['win_ratio'] = 0
            if games_played > 0:
                stats['win_ratio'] = int((float(games_won) / float(games_played)) * 100.0)
        
            team['stats'] = stats

        return teams
    
    def __str__(self):
        return self.name
    
    def __unicode__(self):
        return self.name
    
    
class Game(models.Model):
    date = models.DateField()
    type = models.CharField(max_length = 30)
    signups = models.ManyToManyField(Player, related_name = 'signups')
    team_0 = models.ManyToManyField(Player, related_name = 'team_0')
    team_1 = models.ManyToManyField(Player, related_name = 'team_1')
    winning_team = models.IntegerField()
    summary = models.TextField(default = '')
    
    
    def __str__(self):
        return self.type + ' - ' + str(self.date)
    
    def __unicode__(self):
        return self.type + ' - ' + str(self.date)
