var dl_app = angular.module('dl_app', ['ngRoute']);


dl_app.factory('games_factory', function($http) {
    function get(callback) {
        $http({
            method: 'GET',
            url: '/dagenslag/api/games?filter=year',
            cache: true
        }).success(callback);
    }

    return {
        list: get
    }
});


dl_app.factory('players_factory', function($q, $http) {
    var selected_players = [];

    function get(callback) {
        $http({
            method: 'GET',
            url: '/dagenslag/api/players?filter=year',
            cache: true 
        }).success(function(players) {
            for (var j = 0; j < players.length; j++) {
                players[j].selected = false;
                for (var i = 0; i < selected_players.length; i++) {
                    if (selected_players[i].id == players[j].id) {
                        players[j].selected = true;
                    }
                }
            }
            callback(players);
        });
    }

    function get_selected(callback) {
        callback(selected_players); 
    }

    function find(nickname, callback) {
        get(function(players) {
            var player = players.filter(function(entry) {
                return entry.nickname === nickname;
            })[0];
            callback(player);
        });
    }

    function select(nickname) {
        return $q(function(resolve, reject) {
            find(nickname, function(player) {
                if (!player) {
                    reject('Could not find player: ' + nickname);
                }
                selected_players.push(player);
                resolve(player);
            });
        });
    }

    function deselect(nickname) {
        return $q(function(resolve, reject) {
            find(nickname, function(player) {
                for (var i = 0; i < selected_players.length; i++) {
                    if (selected_players[i].id === player.id) {
                        selected_players.splice(i, 1);
                        resolve();
                    }
                }
            });
            reject('Could not deselect: ' + nickname);
        });

    }

    function select_all() {
        get(function(players) {
            for (var j = 0; j < players.length; j++) {
                players[j].selected = true;
            }
            selected_players = players;
        });
    }

    function clear_selected() {
        get(function(players) {
            for (var j = 0; j < players.length; j++) {
                players[j].selected = false;
            }
        });
        selected_players = [];
    }

    return {
        list: get,
        list_selected: get_selected,
        find: find,
        select: select,
        deselect: deselect,
        select_all: select_all,
        clear_selected: clear_selected
    }
});

dl_app.factory('gametypes_factory', function($http) {
    function get(callback) {
        $http({
            method: 'GET',
            url: '/dagenslag/api/gametypes',
            cache: true
        }).success(callback);
    }

    return {
        list: get
    }
});

dl_app.factory('teams_factory', function($http) {
    function create(num_teams, players, callback) {


        var players_csv = '';
        for (var i = 0; i < players.length; i++) {
            players_csv += players[i].nickname + ',';
        }


        $http({
            method: 'GET',
            url: '/dagenslag/api/teams/' + num_teams + '/' + players_csv,
            cache: true
        }).success(callback);
    }

    return {
        create: create 
    }
});

dl_app.controller('games_controller', function($scope, games_factory) {
    $scope.loading = true;
    games_factory.list(function(games) {
        $scope.games = games;
        $scope.loading = false;
    });
});

dl_app.controller('select_controller', function($scope, players_factory) {
    $scope.select_players = function() {
        players_factory.clear_selected();
        var signups = $scope.game.signups;
        for (var i = 0; i < signups.length; i++) {
            players_factory.select(signups[i].nickname);
        } 
    }
});

dl_app.controller('signup_controller', function($scope, $http) {
    $scope.submit = function() {
        var player = $scope.player;
        var game = $scope.game;
        if (player) {
            var i = game.unsigned.indexOf(player);
            game.unsigned.splice(i, 1);
            game.signups.push(player);
            
            var data = { nickname : player.nickname };
            var resp = $http.post('/dagenslag/api/signup/' + game.id, data, { headers : { 'Content-Type' : 'application/json' } });
        }
    }
});

dl_app.controller('players_controller', function($scope, players_factory, gametypes_factory) {
    $scope.loading = true;
    players_factory.list(function(players) {
        $scope.players = players

        var all_selected = true;
        for (var i = 0; i < players.length; i++) {
            if (!players[i].selected) {
                all_selected = false;
                break;
            }
        }

        $scope.players_selected = all_selected;
        $scope.loading = false;
    });

    gametypes_factory.list(function(gametypes) {
        $scope.gametypes = gametypes 
    });

    $scope.year = (new Date().getFullYear()) 

    $scope.select_all = function() {
        $scope.players_selected = !$scope.players_selected;

        angular.forEach($scope.players, function(player) {
            player.selected = $scope.players_selected;
        });

        if ($scope.players_selected) {
            players_factory.select_all();
        }
        else {
            players_factory.clear_selected(); 
        }
    }

    $scope.select_checkbox = function(player) {
        if (player.selected) {
            players_factory.select(player.nickname);
        }
        else {
            players_factory.deselect(player.nickname);
        }
    }
});

dl_app.controller('player_controller', function($scope, $routeParams, $location, players_factory) {
    $scope.loading = true;
    players_factory.find($routeParams.player_nickname, function(player) {
        if (player) {
            $scope.player = player
            $scope.loading = false;
        }
        else {
            $location.path('/');
        }
    });
});

dl_app.controller('teams_controller', function($scope, $location, $q, teams_factory, players_factory) {

    $scope.loading = true;
    load_teams = function(players) {
        if (players.length > 0) {
            teams_factory.create(2, players, function(teams) {
                $scope.teams = teams;
                $scope.loading = false;
            });
        }
        else {
            $location.path('/players');
        }
    }
    
    // Check query string if this was a linked teams list
    var query = $location.search();
    if (query.players) {

        var players_csv = query.players.split(',');
        players_csv = players_csv.filter(Boolean);
        var promises = [];
        for (var i = 0; i < players_csv.length; i++) {
            promises.push(players_factory.select(players_csv[i]));
        } 

        // Wait for all selections before continuing
        $q.all(promises).then(function(players) {
            load_teams(players);
        });
    } 

    // Else use the players selected in player list
    else {
        players_factory.list_selected(function(players) {
            load_teams(players);
            
            // Add selected players to query string
            var players_csv = '';
            for (var i = 0; i < players.length; i++) {
                players_csv += players[i].nickname + ',';
            }
            $location.search({'players' : players_csv});
        });
    }
    
});

dl_app.config(['$routeProvider', function($routeProvider) {
    $routeProvider

    .when('/', {
        templateUrl : '/dagenslag/games.html',
        controller : 'games_controller'
    })
    
    .when('/players', {
        templateUrl : '/dagenslag/players.html',
        controller : 'players_controller'
    })

    .when('/player/:player_nickname', {
        templateUrl : '/dagenslag/player.html',
        controller : 'player_controller'
    })

    .when('/teams', {
        templateUrl : '/dagenslag/teams.html',
        controller : 'teams_controller',
        reloadOnSearch: false
    })

    .otherwise({
        redirectTo : '/'  
    });
        
}]);
