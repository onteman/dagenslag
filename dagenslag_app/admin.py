from django.contrib import admin

from dagenslag_app.models import *


# Register your models here.
admin.site.register(Player)
admin.site.register(Game)